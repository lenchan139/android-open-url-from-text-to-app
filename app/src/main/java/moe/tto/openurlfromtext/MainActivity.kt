package moe.tto.openurlfromtext

import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.webkit.URLUtil
import android.widget.Toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //setContentView(R.layout.activity_main)
        val text = intent.getStringExtra(Intent.EXTRA_TEXT)
        if(text != null && text.isNotEmpty()){
            var lastUrl = ""
            val arrText = text.split(" ")
            if(arrText.isNotEmpty()){
                arrText.reversed()
                for(o in arrText){
                    if(URLUtil.isValidUrl(o)){
                        lastUrl = o
                    }
                }
            }
            if(lastUrl.isNotEmpty()){
                //val intent = Intent(Intent.ACTION_VIEW, Uri.parse(lastUrl))
                //val chooser = Intent.createChooser(intent, "Open In")
                //startActivity(chooser)
                runToExternal(lastUrl)
            }else{
                Toast.makeText(this, "There no any valid url.", Toast.LENGTH_SHORT).show()
            }
        }
        // finish this activity no matter include url
        finish()
    }

    fun runToExternal(url: String) {
        // beginning
        val preIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
        val targetedShareIntents = ArrayList<Intent>()
        val browserIntent = Intent.createChooser(preIntent, "Open with...")
        val resInfo = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            packageManager.queryIntentActivities(preIntent, PackageManager.MATCH_ALL)
        } else {
            packageManager.queryIntentActivities(preIntent, PackageManager.MATCH_DEFAULT_ONLY)
        }

        for (resolveInfo in resInfo) {
            Log.v("listV", resolveInfo.activityInfo.packageName)
            val packageName = resolveInfo.activityInfo.packageName
            val targetedShareIntent = Intent(Intent.ACTION_VIEW, Uri.parse(url))
            targetedShareIntent.setPackage(packageName)
            if (!packageName.contains(applicationContext.packageName)) {
                targetedShareIntents.add(targetedShareIntent)
                Log.v("listVTureFalse", "True")
            }
        }
        if (targetedShareIntents.size > 1) {
            val chooserIntent = Intent.createChooser(
                targetedShareIntents.removeAt(targetedShareIntents.size - 1), "Open with...")

            chooserIntent.putExtra(
                Intent.EXTRA_INITIAL_INTENTS, JavaUtils().listToPracelable(targetedShareIntents))
            startActivity(chooserIntent)
        } else if (targetedShareIntents.size == 1) {
            startActivity(targetedShareIntents.get(0))
        } else {
            Toast.makeText(this, "nothing here"
                , Toast.LENGTH_SHORT).show()
        }

    }

}
